Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s1-0011-ourplant_x3_black_2).

| document | download options |
|:-------- | ----------------:|
| operating manual           |[de](https://gitlab.com/ourplant.net/products/s1-0011-ourplant_x3_black_2/-/raw/main/01_operating_manual/S1-0008-0011_D1_BA_OurPlant%20X3.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s1-0011-ourplant_x3_black_2/-/raw/main/02_assembly_drawing/s1-0011_C_ZNB_Ourplant_X3_Black2.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s1-0011-ourplant_x3_black_2/-/raw/main/S1-0008_to_S1-0011_A_EPLAN_OurPlant%20X3.pdf)|
|maintenance instructions   ||
|spare parts                ||

