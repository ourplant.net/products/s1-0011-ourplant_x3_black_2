

**OurPlant X3 black²**
Bei der OurPlant X3 black² werden die Achsen aus Karbon gefertigt. Das geringere Achsgewicht hat zur Folge, dass Produkte auf dieser Anlage schneller bearbeitet werden können. Aufgrund der positiven Materialeigenschaften des Karbons erhöht sich auch die Präzision der Bearbeitungsmodule. Dank einer zweiten Y-Achse und einer Vielzahl von Bearbeitungsmodulen ist die X3 black² in der Lage unterschiedlichste Technologien und komplexe Prozesse parallel abzubilden. Die Taktzeit verringert sich somit nochmals.

Die OurPlant X3 ist eine Minifabrik für die Mikromontage, die hochgradig individuelle und komplexe Prozesse mit hoher Geschwindigkeit kombiniert. Wie alle Maschinen der OurPlant-Familie basiert die Mikromontageanlage OurPlant X3 auf einem modularen Aufbau. Die Maschine kombiniert hochgradig individuelle und komplexe Prozesse mit hoher Geschwindigkeit. Dank der hohen Variabilität von Bearbeitungsköpfen und Grundplattenmodulen ermöglicht die OurPlant X3 höchste Prozessvielfalt und -flexibilität. Die Anlage ermöglicht die Integration von Bearbeitungsköpfen mit einem außergewöhnlichen Z-Hub von 150 mm und ist somit für Aufgaben der 3D-Mikromontage konzipiert.

**Vorteile:**
– Ideal für mittlere bis große Losgrößen
– Geringe Rüstaufwände durch echte Plug & Play-Fähigkeit
– Einfache Integration in eine Fertigungslinie durch Einsatz eines Transportsystems
– Softwarebasierte Systemsteuerung über Touchscreen

**Technische Informationen:**
– Bearbeitungsköpfe mit integrierter Z-Achse
– pro Portal ein Interface mit 10 elektrischen Anschlüssen (5x CAN, 5x Ethernet) für die Aufnahme von Bearbeitungsmodulen bis zu einer Gesamtbreite von 250 mm
– Bedienpanel
– Software und Steuerung mit integriertem Industrie-PC

 

**Varianten der OurPlant X3:**

**OurPlant X3 blue**
Das Achssystem ist aus Aluminium gefertigt und ist im Vergleich zu der Black-Edition die kostengünstigere Variante.

**OurPlant X3 blue²**
Es besteht die Möglichkeit die OurPlant X3 blue mit einer zweiten Y-Achse auszustatten. Auf der OurPlant X3 blue² kann auf 2 unabhängig voneinander arbeitenden Y-Achsen die doppelte Anzahl an Bearbeitungsmodulen zum Einsatz kommen. Das hat den Vorteil, dass komplexe Prozesse parallel abgebildet werden können und somit die Taktzeit verringert wird. Das Achssystem ist aus Aluminium gefertigt und ist im Vergleich zu der Black-Edition die kostengünstigere Variante.

**OurPlant X3 black**
Die OurPlant X3 gibt es auch in der Leicht-Variante mit der OurPlant X3 black. Bei der Black-Edition werden die Achsen aus Karbon gefertigt. Das geringere Achsgewicht hat zur Folge, dass Produkte auf dieser Anlage schneller bearbeitet werden können. Aufgrund der positiven Materialeigenschaften des Karbons erhöht sich auch die Präzision der Bearbeitungsmodule.

**Erweiterung**:

**Alle Varianten der OurPlant X3 können mit einer Laserschutzeinhausung für Laser der Wellenlänge 940 nm – 1065 nm ausgerüstet werden.

Alle Varianten des OurPlant X3 werden optional mit UL-Zertifizierung angeboten.